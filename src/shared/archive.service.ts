import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {ArchiveEntity} from "../model/archive";

@Injectable()
export class ArchiveService {

  constructor(private http: Http) {
  }

  private url = 'http://localhost:8083/api/archive';

  findAll(): Observable<ArchiveEntity[]> {
    return this.http.get(this.url).map((res: Response) => res.json());
  }


}
