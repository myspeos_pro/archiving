import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {ModuleEntity} from '../model/module';

@Injectable()
export class ModuleService {

  constructor(private http: Http) {
  }

  private url = '/api/modules';

  findAll(): Observable<ModuleEntity[]> {
    return this.http.get(this.url).map((res: Response) => res.json());
  }


}
