import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpModule} from '@angular/http';
import {ArchivingComponent} from './archiving.component';
import {ArchiveService} from '../../shared/archive.service';


@NgModule({
  declarations: [
    ArchivingComponent
  ],
  imports: [
    HttpModule,
    BrowserModule
  ],
  providers: [ArchiveService]
})
export class ArchivingModule { }
