import {Component, OnInit} from '@angular/core';
import {routerTransition} from '../router.animations';
import {ArchiveEntity} from '../../model/archive';
import {ArchiveService} from '../../shared/archive.service';

@Component({
  selector: 'archiving-app',
  templateUrl: './archiving.component.html',
  styleUrls: ['./archiving.component.css'],
  animations: [routerTransition()],
  host: {'[@routerTransition]': ''}
})
export class ArchivingComponent implements OnInit {

  public archiveList: ArchiveEntity[];

  constructor(private service: ArchiveService) {
  }

  ngOnInit() {
    this.service.findAll().subscribe(res => this.archiveList = res);
  }
}
