import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ArchiveService} from '../shared/archive.service';
import {ArchivingComponent} from './archiving/archiving.component';
import {RouterModule} from '@angular/router';
import {appRoutes} from './routes';
import {SlimLoadingBarModule} from 'ng2-slim-loading-bar';
import {ModuleService} from '../shared/module.service';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    SlimLoadingBarModule.forRoot()
  ],
  declarations: [
    AppComponent,
    ArchivingComponent
  ],
  providers: [
    ArchiveService,
    ModuleService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {}
