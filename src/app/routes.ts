import {Routes} from '@angular/router';
import {ArchivingComponent} from "./archiving/archiving.component";

export const appRoutes: Routes = [
  {path: 'home', component: ArchivingComponent},
  {path: '', redirectTo: '/home', pathMatch: 'full'}
]
