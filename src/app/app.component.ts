import {Component, OnInit} from '@angular/core';
import {ModuleEntity} from '../model/module';
import {ModuleService} from '../shared/module.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

  public menuItemList:ModuleEntity[] = [];
  public activeMenu = 'Archiving';

  constructor(private moduleService:ModuleService) {
  }

  ngOnInit():void {
    this.moduleService.findAll().subscribe((list) => {
      this.menuItemList = list;
    });
  }
}
